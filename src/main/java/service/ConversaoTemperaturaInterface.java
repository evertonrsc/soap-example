package service;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
public interface ConversaoTemperaturaInterface {
	@WebMethod
	public double celsiusToFahrenheit(double temperatura);

	@WebMethod
	public double fahrenheitToCelsius(double temperatura);
}

