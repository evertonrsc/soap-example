package service;

import javax.jws.WebService;

@WebService(endpointInterface = "service.ConversaoTemperaturaInterface")
public class ConversaoTemperatura implements ConversaoTemperaturaInterface {
	@Override
	public double celsiusToFahrenheit(double temperatura) {
		return temperatura * 9.0 / 5.0 + 32;
	}

	@Override
	public double fahrenheitToCelsius(double temperatura) {
		return (temperatura - 32) * 5.0 / 9.0;
	}
}
