package client;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import service.ConversaoTemperaturaInterface;

public class ClienteServico {
	public static void main(String[] args) {		
		URL endpoint = null;
		try {
			endpoint = new URL("http://localhost:8080/soap-example/ConversaoTemperatura?wsdl");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		QName qualifiedName = new QName("http://service/", "ConversaoTemperaturaService");
		Service service = Service.create(endpoint, qualifiedName);
		ConversaoTemperaturaInterface stub = service.getPort(ConversaoTemperaturaInterface.class);
		
		double temperaturaC = 27.0;
		double temperaturaF = stub.celsiusToFahrenheit(temperaturaC);
		System.out.println(temperaturaC + "°C = " + temperaturaF + "°F");
	}
}
